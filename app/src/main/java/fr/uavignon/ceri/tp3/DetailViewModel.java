package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();
    private MutableLiveData<Throwable> webServiceThrowable;
    private MutableLiveData<Boolean> isLoading;
    private WeatherRepository repository;
    private MutableLiveData<City> city;

    public DetailViewModel (Application application) {
        super(application);
        isLoading =new MutableLiveData<>(false);// repository.getIsLoading();
        //new MutableLiveData<>(false);
        webServiceThrowable =new MutableLiveData<>(); //repository.getWebServiceThrowable();
        //new MutableLiveData<>();
        repository = WeatherRepository.get(application);
        city = new MutableLiveData<>();
    }

    public void setCity(long id) {
        repository.getCity(id);
        city = repository.getSelectedCity();
        isLoading=repository.getIsLoading();
        webServiceThrowable = repository.getWebServiceThrowable();
    }
    LiveData<City> getCity() {
        return city;
    }
    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

   /* void resetWebServiceThrowable() {
        repository.resetWebServiceThrowable();
        webServiceThrowable = repository.getWebServiceThrowable();
    }*/

    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public void loadWeatherCity() {
        repository.loadWeatherCity(city.getValue());
    }
}


