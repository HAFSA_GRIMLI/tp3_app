package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class ListViewModel extends AndroidViewModel {
    private MutableLiveData<Throwable> webServiceThrowable;
    private MutableLiveData<Boolean> isLoading;
    private WeatherRepository repository;
    private LiveData<List<City>> allCities;

    public ListViewModel (Application application) {
        super(application);
        repository = WeatherRepository.get(application);
        allCities = repository.getAllCities();
        isLoading = new MutableLiveData<>(false);
        //new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
    }
    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }

  /*  void resetWebServiceThrowable() {
        repository.resetWebServiceThrowable();
        webServiceThrowable = repository.getWebServiceThrowable();
    }*/

    public void setVariable() {
        isLoading=repository.getIsLoading();
        webServiceThrowable = repository.getWebServiceThrowable();

    }
    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }
    /*public void loadWeatherAllCities() {
        repository.loadWeatherAllCities();
    }
*/
    LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public void deleteCity(long id) {
        repository.deleteCity(id);
    }


}
