package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;
    public static volatile int nbAPIloads = 1;
    private List<City> synCities;
    private CityDao cityDao;

    private MutableLiveData<Throwable> webServiceThrowable;
    private MutableLiveData<Boolean> isLoading;
    private final OWMInterface api;
    private static volatile WeatherRepository INSTANCE;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {
        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();

        isLoading = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>(null);
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org/data/2.5/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }

    public MutableLiveData<Boolean> getIsLoading() { return isLoading; }

    public MutableLiveData<Throwable> getWebServiceThrowable() {
        return webServiceThrowable;
    }

    public void resetWebServiceThrowable() {
        webServiceThrowable = new MutableLiveData<>();
    }



    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadWeatherCity(City city) {
        isLoading.postValue(true);
        api.getWeather(city.getName()+",,"+city.getCountryCode(), "a8df6efbd32784e2a498be33afcce664").enqueue(
                new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call,
                                           Response<WeatherResponse> response) {
                        if(nbAPIloads==1) {
                            isLoading.postValue(false);
                        }
                        else{
                            nbAPIloads=nbAPIloads-1;
                        }
                        WeatherResult.transferInfo(response.body(), city);
                        updateCity(city);
                        Log.d("onResponse", response.toString());

                        /*  isLoading.postValue(false);
                        try {
                            WeatherResult.transferInfo(response.body(), city);
                            updateCity(city);
                        } catch (Throwable t) {
                            onFailure(call, t); 
                        }*/
                 //       Log.d("res", city.toString());
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
//                        webServiceThrowable.postValue(new Throwable(t.getMessage()));
                        if(nbAPIloads==1) {
                            isLoading.postValue(false);
                        }
                        else{
                            nbAPIloads=nbAPIloads-1;
                        }
                        webServiceThrowable.postValue(t);
                        Log.d("onFailure", t.getMessage());
                    }
                });
    }

    public void loadWeatherAllCities() {
      /*  for (City city : allCities.getValue()) {
            loadWeatherCity(city);
        }*/
       new Thread(new Runnable() {
            @Override
            public void run() {
                synCities = cityDao.getSynchrAllCities();
                nbAPIloads=synCities.size();
                for(int i=0 ; i<nbAPIloads; i++){
                    loadWeatherCity(synCities.get(i));
                }
            }
        }).start();

    }


}
