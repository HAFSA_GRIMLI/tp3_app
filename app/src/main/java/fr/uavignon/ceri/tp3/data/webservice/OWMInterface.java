package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import retrofit2.Call;
public interface OWMInterface {
    //@Headers("Accept: application/json")
    @GET("weather")
    public Call<WeatherResponse> getWeather(@Query("q") String query, @Query("APIkey") String apiKey);
}
