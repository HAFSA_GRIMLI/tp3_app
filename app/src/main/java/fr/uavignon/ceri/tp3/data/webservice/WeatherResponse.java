package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {


/*
    }
    public class Clouds
    {
        public int All { get; set; }
    }*/
  public final List<Conditions> weather = null;

    public final Long dt = null;
    public final Main main = null;
    public final Wind wind = null;
    public final Clouds clouds = null;



    public static class Conditions {
        public final String description = null;
        public final String icon = null;
    }

    public static class Main {
        public final Float temp = null;
        public final Integer humidity = null;
    }
    public static class Wind {
        public final Integer speed = null;
        public final Integer deg = null;
    }
    public static class Clouds {
        public final Integer all = null;
    }
}
